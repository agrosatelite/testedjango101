# Teste AgrosatÚlite

## Requirements

 - Python 3.5
 - Virtual Enviroment
 - Django
 
 ## RUN

Create your virtual environment. You can create using Virtualenv (apt install virtualenv):
```shell
$ virtualenv env -p python3
$ source env/bin/activate
```

Upgrade pip and setuptools

```shell
$ pip3 install --upgrade pip
$ pip3 install setuptools --upgrade
```

Inside the project directory, run the command to install the env requirements:
```shell
$ pip3 install -r requirements.txt
```

Execute the system:
```shell
$ python manage.py runserver
```

Complete the implementation so that:

- Negative product values are not allowed
- You create a route/endpoint for editing the product
- You create a route/endpoint for returning the total value of the basket
- It is possible to choose how products are ordered: alphabetically or ordered by their value, ascending or descending.
 
After you have the system implemented and running, do the following tasks:

- Create two products
- Create two clients
- Create a basket for each registered client

After you are done with the tasks:
- Zip your project folder. Send it through email for the Agrosatelite's contact through which you received this test.
- In the body/message of that email, inform how many products and how many clients are registed in the application once all the test is complete.
 





