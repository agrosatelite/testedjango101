# -*- coding: utf-8 -*-
from django.db import models


class Client(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    value = models.FloatField()

    def __str__(self):
        return self.name


class Basket(models.Model):
    product = models.ManyToManyField('Product')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return self.product
