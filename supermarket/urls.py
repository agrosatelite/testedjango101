from __future__ import unicode_literals

from django.urls import path

from .views import ProductListCreateView, ProductRetrieveUpdateDestroyAPIView, \
    BasketRetrieveUpdateDestroyAPIView, BasketListCreateView, \
    ClientListCreateView, ClientRetrieveUpdateDestroyAPIView

urlpatterns = (
    path('products', ProductListCreateView.as_view(),
         name="products-list"),
    path('products/<int:pk>',
         ProductRetrieveUpdateDestroyAPIView.as_view(),
         name="products-retrieve"),

    path(
        'baskets/',
        BasketListCreateView.as_view(),
        name="baskets-list-create"
    ),
    path(
        'baskets/<int:pk>',
        BasketRetrieveUpdateDestroyAPIView.as_view(),
        name="baskets-retrieve"
    ),

    path(
        'clients', ClientListCreateView.as_view(),
        name="clients-list"
    ),
    path(
        'clients/<int:pk>', ClientRetrieveUpdateDestroyAPIView.as_view(),
        name="clients-retrieve"
    )
)
